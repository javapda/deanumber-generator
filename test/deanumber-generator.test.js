const DeaNumberGenerator = require('../src/deanumber-generator')
const deaNumberGenerator = new DeaNumberGenerator();
/**
 * 
 * A/B/F/G – Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy
 * M – Mid-Level Practitioner (NP/PA/OD/ET, etc.)
 * P/R – Manufacturer/Distributor/Researcher/Analytical Lab/Importer/Exporter/Reverse Distributor/Narcotic Treatment Program
 *
 */
describe('DeaNumberGenerator Tests', () => {
  it('should instantiate', () => {
    expect(new DeaNumberGenerator()).toBeTruthy()
  })
  test('DeaNumberGenerator generateRandomDeaNumber', () => {
    expect(deaNumberGenerator.generateRandomDeaNumber()).toBeTruthy()
  })
  test('DeaNumberGenerator parse', () => {
    const parsed = deaNumberGenerator.parse("GD0865937");
    expect(parsed).toBeTruthy();
    // console.log(JSON.stringify(parsed));
  })
  test('DeaNumberGenerator test valid dea number', () => {
    expect(deaNumberGenerator.isValid("AZ1991377")).toBe(true);
    expect(deaNumberGenerator.isValid("GQ6917883")).toBe(true);
    expect(deaNumberGenerator.isValid("GD0865937")).toBe(true);
    expect(deaNumberGenerator.isValid("XD0865937")).toBe(false);
  });
  test('DeaNumberGenerator test null dea number', () => {
    expect(() => deaNumberGenerator.isValid(undefined)).toThrow();
    expect(() => deaNumberGenerator.isValid(null)).toThrow();
  });
  test('generateRandomDeaNumber', () => {
    expect(deaNumberGenerator.generateRandomDeaNumber()).toBeTruthy();
  });
  test('test _notNullNotUndefined', () => {
    expect(() => deaNumberGenerator._notNullNotUndefined(undefined)).toThrow();
    expect(() => deaNumberGenerator._notNullNotUndefined(null)).toThrow();
    expect(() => deaNumberGenerator._notNullNotUndefined('jed')).not.toThrow();
    expect(() => deaNumberGenerator._notNullNotUndefined('')).not.toThrow();
  });
  test('test _checkDigitMatch', () => {
    expect(deaNumberGenerator._checkDigitMatch('AD0865937'));
  });
  test('_random6Digits', () => {
    let min = 999999;
    let i;
    for (i = 0; i < 99999; i++) {
      const r6 = deaNumberGenerator._random6Digits();
      const r6Int = parseInt(r6);
      if (r6Int < min) {
        min = r6Int;
      }
    }
    expect(min).toBeLessThan(1000000);
    expect(min).toBeGreaterThan(99999);
    expect(deaNumberGenerator._calcCheckDigit("123456")).toBeTruthy();
    expect(() => deaNumberGenerator._calcCheckDigit("12345")).toThrow();
    expect(() => deaNumberGenerator._calcCheckDigit("123456789")).toThrow();
    expect(() => deaNumberGenerator._calcCheckDigit(undefined)).toThrow();
    expect(() => deaNumberGenerator._calcCheckDigit(null)).toThrow();
  });
  test('print out', () => {
    //console.log(JSON.stringify(deaNumberGenerator));
  });
  test('_randomRegistrantType', () => {
    expect(deaNumberGenerator._randomRegistrantType()).toBeTruthy();
  });
  
  test('generate - random dea number', () => {
    expect(deaNumberGenerator.generateRandomDeaNumber()).toBeTruthy();
    // const deaNumber=deaNumberGenerator.generateRandomDeaNumber();
    // console.log(`deaNumber: ${deaNumber}`);
  });
})

describe("hidden functions",()=>{
  it('_randomFirstDigitPractitioner', () => {
    expect(deaNumberGenerator._randomFirstDigitPractitioner()).toBeTruthy()
  })
  it('_randomFirstDigitMidLevelPractitioner',()=>{
    expect(deaNumberGenerator._randomFirstDigitMidLevelPractitioner()).toBeTruthy()
  })
  it('_randomFirstDigitDod',()=>{
    expect(deaNumberGenerator._randomFirstDigitDod()).toBeTruthy()
  })
  it('generateRandomDeaNumberDod',()=>{
    expect(deaNumberGenerator.generateRandomDeaNumberDod()).toBeTruthy()
  })
  it('generateRandomDeaNumberMidLevelPractitioner',()=>{
    expect(deaNumberGenerator.generateRandomDeaNumberMidLevelPractitioner()).toBeTruthy()
  })
  it('generateRandomDeaNumberPractitioner',()=>{
    expect(deaNumberGenerator.generateRandomDeaNumberPractitioner()).toBeTruthy()
  })
  
})
