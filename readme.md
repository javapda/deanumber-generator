# [deanumber-generator](https://bitbucket.org/javapda/deanumber-generator)

# Installation / setup #

## install CLI ##
```
 npm install -g deanumber-generator
```

## update CLI ##
```
 npm update -g deanumber-generator
```

## CLI usage ##
```
deanumber-generator --help
deanumber-generator --number 3 --deanumber
deanumber-generator --number 3 --deanumber
deanumber-generator --deanumber-practitioner
```

## NPM usage ##
```
npm run help 
npm run deanumber
npm run deanumber-practitioner
npm run deanumber-midlevel-practitioner
npm run deanumber-dod
```

## Code usage ##
* try it out by going to [RunKit for deanumber-generator](https://npm.runkit.com/deanumber-generator) and replacing the contents with those below, then click the 'run' button
```
const {DeaNumberGenerator} = require('deanumber-generator');
const deaNumberGenerator = new DeaNumberGenerator();

console.log(`*** Generate random DEA Numbers of various kinds ***`);
console.log(`dea number: ${deaNumberGenerator.generateRandomDeaNumber()}`);
console.log(`dea number practitioner: ${deaNumberGenerator.generateRandomDeaNumberPractitioner()}`);
console.log(`dea number mid-level practitioner: ${deaNumberGenerator.generateRandomDeaNumberMidLevelPractitioner()}`);
console.log(`dea number dod: ${deaNumberGenerator.generateRandomDeaNumberDod()}`);
```
## output (example) ##
```
*** Generate random DEA Numbers of various kinds ***
dea number: BG7845071
dea number practitioner: AL7584457
dea number mid-level practitioner: MX6261135
dea number dod: GO3381528

```

## sample DEA Numbers ##
### sample practitioner DEA Number ###
```
deanumber-generator --deanumber-practitioner
BI1126627
FV2783961
AG3585253
```
### sample mid-level practitioner DEA Number ###
```
deanumber-generator --deanumber-midlevel-practitioner
MF6367557
```

### sample mid-level practitioner DEA Number ###
```
deanumber-generator --deanumber-dod
GU3012438
```

## registrant types (first letter of DEA number)
* A/B/F/G – Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy
* M – Mid-Level Practitioner (NP/PA/OD/ET, etc.)
* P/R – Manufacturer/Distributor/Researcher/Analytical Lab/Importer/Exporter/Reverse Distributor/Narcotic Treatment Program

## dea number format from [HERE](https://en.wikipedia.org/wiki/DEA_number)
* 2 letters, 6 numbers, and 1 check digit

# Links #
* [What are dea numbers?](https://www.meditec.com/blog/dea-numbers-what-do-they-mean)
* [dod dea number starts with D](https://deadiversion.usdoj.gov/drugreg/index.html)
* [dea number wikipedia](https://en.wikipedia.org/wiki/DEA_number)
* [list of controlled substances](https://www.deadiversion.usdoj.gov/schedules/)
* [DEA Number Verification Formula](https://student.passassured.com/flash/section_tutorials/learn/fed%20law_4_web_old/2.html)

# Dev-Links #
* [jest (for testing)](https://jestjs.io/) | [jest api](https://jestjs.io/docs/api)

# Terminology
* _DEA_ : Drug Enforcement Agency
* _DEA Number_ : DEA Number

# Notes
* DEA numbers are required for any drugs that are classified Schedule II-V under the Controlled Substances Act.
* Schedule I drugs (e.g. heroin, lysergic acid diethylamide (LSD), marijuana (cannabis), peyote, methaqualone, and 3,4-methylenedioxymethamphetamine ("Ecstasy")) are completely illegal at the federal level and have no accepted medical use.

# How to publish
```
// after git committing
npm version patch
npm publish --access public
```

# Future Improvements #
* add last name option (used for determining 2nd letter in dea number)
* add is-business option (an alternative to last name)
* [move to promises (async/await)](https://nodejs.dev/learn/modern-asynchronous-javascript-with-async-and-await)


# releases #
## 0.0.5 (11/12/2021)
* option: --last-name <last_name>
  * will honor the 2nd digit based on last name
* option: --registrant-uses-business-address
  * will honor the 2nd digit based on presumption the registrants will use a business address instead of last name