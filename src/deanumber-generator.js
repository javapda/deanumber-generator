/**
 * https://deadiversion.usdoj.gov/drugreg/index.html
 *
 * Registrant type (first letter of DEA Number):
 *
 * A/B/F/G – Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy
 * M – Mid-Level Practitioner (NP/PA/OD/ET, etc.)
 * P/R – Manufacturer/Distributor/Researcher/Analytical Lab/Importer/Exporter/Reverse Distributor/Narcotic Treatment Program
 * 
 * https://www.meditec.com/blog/dea-numbers-what-do-they-mean
 
B = Hospital/Clinic
C = Practitioner (i.e., a physician, dentist, veterinarian)
D = Teaching Institution
E = Manufacturer
F = Distributor
G = Researcher
H = Analytical Lab
J = Importer
K = Exporter
L = Reverse Distributor (an entity that collects expired or unwanted drugs for disposal)
M = Mid-Level Practitioner (i.e., nurse practitioners, physician's assistants)
P, R, S, T, U = Narcotic Treatment Program
X = Suboxone/Subutex Prescribing Program
A = used by some older entities, last issued in 1985


 * 
 * Some valid DEA Numbers:
 * AD0865937
 * 
 */
module.exports = class DeaNumberGenerator {
  constructor(lastName, registrantIsBusinessAddress) {
    this.lastName=lastName;
    this.secondCharacter=registrantIsBusinessAddress?'9':(lastName&&lastName.length>0?lastName.charAt(0).toUpperCase():undefined);
    this.registrantIsBusinessAddress=registrantIsBusinessAddress;
    this.name = "DeaNumberGenerator";
    this.alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ9"; // 9 is for special case
    this.dodRegistrantTypes = [{ "letter": "G", "info": { "category": "Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy", "dod": true } }];
    this.midlevelRegistrantTypes = [
      { "letter": "M", "info": { "category": "Mid-Level Practitioner (NP/PA/OD/ET, etc.)" } },
    ]
    this.practitionerRegistrantTypes = [
      { "letter": "A", "info": { "category": "Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy" } },
      { "letter":"B", "info": { "category": "Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy" } },
      { "letter":"F","info": { "category": "Hospital/Clinic/Practitioner/Teaching Institution/Pharmacy" } },
    ]
    this.manufacturerRegistrantTypes = [
      { "letter":"P","info": { "category": "Manufacturer/Distributor/Researcher/Analytical Lab/Importer/Exporter/Reverse Distributor/Narcotic Treatment Program" } },
      { "letter":"R","info": { "category": "Manufacturer/Distributor/Researcher/Analytical Lab/Importer/Exporter/Reverse Distributor/Narcotic Treatment Program" } }
    ]
    this.registrantTypes = []
      .concat(this.dodRegistrantTypes)
      .concat(this.midlevelRegistrantTypes)
      .concat(this.practitionerRegistrantTypes)
      .concat(this.manufacturerRegistrantTypes);

  }
  _calcCheckDigit(digits6) {
    if (digits6 && digits6.length == 6) {
      const sum1 = parseInt(digits6.charAt(0)) + parseInt(digits6.charAt(2)) + parseInt(digits6.charAt(4))
      const sum2 = parseInt(digits6.charAt(1)) + parseInt(digits6.charAt(3)) + parseInt(digits6.charAt(5))
      const sum2Times2 = 2 * sum2;
      const sum3 = sum1 + sum2Times2;
      const checkDigit = sum3 % 10;
      return checkDigit;
    } else if (digits6 === undefined) {
      throw new Error('digits6 was undefined');
    } else if (digits6 === null) {
      throw new Error('digits6 was null');
    } else if (digits6.length < 6) {
      throw new Error(`LT:digits6.length was ${digits6.length} but expected 6`);
    } else if (digits6.length > 6) {
      throw new Error(`GT:digits6.length was ${digits6.length} but expected 6`);
    }
  }
  _checkDigitMatch(digits6, checkDigit) {
    let match = false;
    if (digits6 && digits6.length == 6 && checkDigit && checkDigit.length == 1) {
      match = parseInt(checkDigit) == this._calcCheckDigit(digits6);
    }
    return match;
  }
  _notNullNotUndefined(deaNumber) {
    if (deaNumber === undefined) {
      throw new Error('deaNumber undefined');
    } else if (deaNumber === null) {
      throw new Error('deaNumber is null');
    }
    return true;
  }
  isValid(deaNumber) {
    return this._notNullNotUndefined(deaNumber) && this.parse(deaNumber).valid;
  }
  parse(deaNumber) {
    this._notNullNotUndefined(deaNumber)
    const deaNumberRegExp = /([A|B|F|G])([A-Z])(([0-9]{6})([0-9]))/;
    let registrantType = '?';
    let firstLetterOfLastName = '?';
    let digits7 = '#######';
    let digits6 = '######';
    let checkDigit = '#';
    let valid = false
    let checkDigitMatch = false;
    if (deaNumberRegExp.test(deaNumber)) {
      const match = deaNumber.match(deaNumberRegExp);
      // console.log(`match ${deaNumber}:${match}`);
      registrantType = match[1];
      firstLetterOfLastName = match[2];
      digits7 = match[3];
      digits6 = match[4];
      checkDigit = match[5];
      checkDigitMatch = this._checkDigitMatch(digits6, checkDigit);
      valid = checkDigitMatch
    }
    return {
      deaNumber,
      registrantType,
      firstLetterOfLastName,
      digits7,
      digits6,
      checkDigit,
      valid,
      checkDigitMatch
    }
  }
  _random6Digits() {
    return Math.floor(100000 + Math.random() * 900000) + "";
  }
  _randomFirstDigit() {
    return this._randomRegistrantType().letter;
  }
  _randomFirstDigitPractitioner() {
    return this._randomRegistrantTypePractitioner().letter;
  }
  _randomFirstDigitMidLevelPractitioner() {
    return this._randomRegistrantTypeMidLevelPractitioner().letter;
  }
  _randomFirstDigitDod() {
    return this._randomRegistrantTypeDod().letter;
  }
  _secondLetter() {
    //console.log(`this.secondCharacter:  ${this.secondCharacter}`);
    return this.secondCharacter ? this.secondCharacter : _randomSecondLetter();
  }
  _randomSecondLetter() {
    return this.alphabet.charAt(Math.floor(Math.random() * this.alphabet.length));
  }
  _randomRegistrantType() {
    return this.registrantTypes[Math.floor(Math.random() * this.registrantTypes.length)];
  }
  _randomRegistrantTypePractitioner() {
    return this.practitionerRegistrantTypes[Math.floor(Math.random() * this.practitionerRegistrantTypes.length)];
  }
  _randomRegistrantTypeMidLevelPractitioner() {
    return this.midlevelRegistrantTypes[Math.floor(Math.random() * this.midlevelRegistrantTypes.length)];
  }
  _randomRegistrantTypeDod() {
    return this.dodRegistrantTypes[Math.floor(Math.random() * this.dodRegistrantTypes.length)];
  }
  
  _buildDeaNumber(registrantTypeLetter,firstLetterOfLastName,digits6,checkDigit){
    return `${registrantTypeLetter}${firstLetterOfLastName}${digits6}${checkDigit}`;
  }
  generateRandomDeaNumberDod() {
    const registrantTypeLetter = this._randomFirstDigitDod();
    const firstLetterOfLastName = this._secondLetter();
    const digits6=this._random6Digits();
    const checkDigit=this._calcCheckDigit(digits6);
    return this._buildDeaNumber(registrantTypeLetter,firstLetterOfLastName,digits6,checkDigit)

  }
  generateRandomDeaNumberMidLevelPractitioner() {
    const registrantTypeLetter = this._randomFirstDigitMidLevelPractitioner();
    const firstLetterOfLastName = this.secondCharacter ? this.secondCharacter : this._secondLetter();
    const digits6=this._random6Digits();
    const checkDigit=this._calcCheckDigit(digits6);
    return this._buildDeaNumber(registrantTypeLetter,firstLetterOfLastName,digits6,checkDigit)

  }
  generateRandomDeaNumberPractitioner() {
    const registrantTypeLetter = this._randomFirstDigitPractitioner();
    const firstLetterOfLastName = this._secondLetter();
    const digits6=this._random6Digits();
    const checkDigit=this._calcCheckDigit(digits6);
    return this._buildDeaNumber(registrantTypeLetter,firstLetterOfLastName,digits6,checkDigit)
  }
  generateRandomDeaNumber() {
    const registrantTypeLetter = this._randomFirstDigit();
    const firstLetterOfLastName = this._secondLetter();
    const digits6=this._random6Digits();
    const checkDigit=this._calcCheckDigit(digits6);
    return this._buildDeaNumber(registrantTypeLetter,firstLetterOfLastName,digits6,checkDigit)
  }

}