#! /usr/bin/env node
const { Command, Option } = require('commander');
const chalk = require('chalk');

// console.log(`file name: ${__filename}`);

const programVersion=require('./package.json').version
const programName=require('./package.json').name
let numberOfDeaNumbers=1;
let lastName=undefined
let secondCharacter=undefined
let registrantIsBusinessAddress=false
let verbose=true
const DeaNumberGenerator = require('./src/deanumber-generator');
const { underline } = require('chalk');
exports.DeaNumberGenerator = DeaNumberGenerator; 

const program = new Command(chalk.bold.yellow(programName));
program.description(`generate various ${chalk.underline('DEA Numbers')}`);
program.version(programVersion);
program.addOption(new Option('-v, --verbose', 'timeout in seconds').default(verbose, `default is ${verbose}`));
program.addOption(new Option('-n, --number <count>', `number ${chalk.bold.blue('(DEA Number)')} of npi(s) to output`).default(numberOfDeaNumbers, 'one DEA Number'))
program.addOption(new Option('--deanumber', 'random dea number'));
program.addOption(new Option('--deanumber-practitioner', 'random practitioner dea number'));
program.addOption(new Option('--deanumber-midlevel-practitioner', `random ${chalk.bold.cyanBright('mid-level')} practitioner dea number`));
program.addOption(new Option('--deanumber-dod', `random dod (${chalk.bold.green('DOD = Department of Defense')}) practitioner dea number`));
program.addOption(new Option('--last-name <last_name>', 'seed the last name - which populates 2nd character in deanumber'));
program.addOption(new Option('--show-parameters', 'outputs known parameters'));
program.addOption(new Option('--registrant-uses-business-address', 'will make the 2nd character a \'9\''));


program.parse(process.argv);
const options = program.opts();
if (options.number) {
  numberOfDeaNumbers=options.number;
}
if (options.lastName && options.lastName.length > 0) {
  lastName=options.lastName;
  secondCharacter=lastName.charAt(0).toUpperCase();
}
if (options.registrantUsesBusinessAddress) {
  registrantIsBusinessAddress=true
  secondCharacter="9";
}
if(!registrantIsBusinessAddress && lastName==undefined) {
  lastName="VeryFakelastName"
}
if (options.deanumber) {
  // console.log(`options.deanmumber time, lastName=${lastName}`);
  // lastName, registrantIsBusinessAddress
  const deaNumberGenerator = new DeaNumberGenerator(lastName, registrantIsBusinessAddress);
  for (let i = 0 ; i < numberOfDeaNumbers; i++) {
    console.log(deaNumberGenerator.generateRandomDeaNumber());
  }
}
if (options.deanumberPractitioner) {
  // const deaNumberGenerator = new DeaNumberGenerator(lastName, registrantIsBusinessAddress);
  const deaNumberGenerator = new DeaNumberGenerator(lastName, registrantIsBusinessAddress);
  for (let i = 0 ; i < numberOfDeaNumbers; i++) {
    console.log(deaNumberGenerator.generateRandomDeaNumberPractitioner());
  }
}
if (options.deanumberMidlevelPractitioner) {
  for (let i = 0 ; i < numberOfDeaNumbers; i++) {
    console.log(deaNumberGenerator.generateRandomDeaNumberMidLevelPractitioner());
  }
}
if (options.deanumberDod) {
  for (let i = 0 ; i < numberOfDeaNumbers; i++) {
    console.log(deaNumberGenerator.generateRandomDeaNumberDod());
  }
}
const _showParamaters = () => {
  const localSecondCharacter=(secondCharacter)
    ?
    ` [2nd character: ${secondCharacter}]`
    :
    "";
  console.log(`
  *** parameters ***
  numberOfDeaNumbers:           ${numberOfDeaNumbers}
  lastName:                     ${lastName}${localSecondCharacter}
  registrantIsBusinessAddress:  ${registrantIsBusinessAddress}
  secondCharacter:              ${secondCharacter}
  `)
}
if (options.showParameters) {
  _showParamaters() 
}

